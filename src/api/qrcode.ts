import * as Http from "../network/http"

const qrcodeApiUrl = "https://api.qrserver.com/v1/create-qr-code"

export default {
  create: (text: string, width: number = 150, height: number = 150) =>
    `${qrcodeApiUrl}?size=${width}x${height}&data=${text}`
}
