import {load} from "react-tooling"
import * as Root from "Root"
import {Route, toUri, fromUri} from "routes"

/*
  Entry point of the application
  This call wire the all cycle

  action -> update -> render
 */
load<Root.State, Root.Action, {}, Route>(
  Root.initState,
  Root.reactsTo,
  Root.update,
  Root.view,
  toUri,
  fromUri,
  module,
  {baseUri: ""}
)
