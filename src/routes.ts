export type Route = Dashboard | NotFound

export enum RouteType {
  NotFound = "NotFound",
  Dashboard = "Dashboard"
}

export interface NotFound {type: RouteType.NotFound}
export const notFound: NotFound = {type: RouteType.NotFound}

export interface Dashboard {type: RouteType.Dashboard}
export const dashboard: Dashboard = {type: RouteType.Dashboard}

export const toUri = (route: Route) => {
  switch (route.type) {
    case RouteType.Dashboard: return ""
    case RouteType.NotFound: return "404"
  }
}

export const fromUri = (uri: string): Route => {
  switch (uri) {
    case "": return dashboard
    default: return notFound
  }
}
