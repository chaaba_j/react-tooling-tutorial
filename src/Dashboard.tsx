import {React, AnyAction, Dispatcher} from "react-tooling"
import QRCodeApi from "./api/qrcode"
import {Localized} from "../lang"
import * as Camera from "./Camera"

export interface State {
  loading: boolean
  qrcodeText: string
  submittedQrCodeText: Maybe<string>
}
export const initState: State = {
  loading: false,
  qrcodeText: "",
  submittedQrCodeText: null
}

export enum ActionType {
  CreateQRCode = "CreateQRCode",
  SetQRCodeText = "SetQRCodeText"
}

export interface SetQRCodeText {
  type: ActionType.SetQRCodeText,
  text: string
}
export const SetQRCodeText = (text: string): SetQRCodeText => ({
  type: ActionType.SetQRCodeText,
  text
})

export interface CreateQRCode {
  type: ActionType.CreateQRCode
}
export const CreateQRCode: CreateQRCode = {type: ActionType.CreateQRCode}

export type Action = CreateQRCode | SetQRCodeText
export const reactsTo = (action: AnyAction): action is Action => {
  switch (action.type) {
    case ActionType.CreateQRCode:
    case ActionType.SetQRCodeText:
      return true
    default:
      return false
  }
}

export const update =
  (state: State, action: Action): State => {
    switch (action.type) {
      case ActionType.CreateQRCode:
        return {...state, submittedQrCodeText: state.qrcodeText}
      case ActionType.SetQRCodeText:
        return {...state, qrcodeText: action.text}
    }
  }

type Dashboard = (props: State & Dispatcher & Localized) => JSX.Element

const Dashboard: Dashboard = ({lang, dispatch, ...state}) => {
  return (
    <div className="dashboard">
      {
        state.loading ? "loading..." : null
      }
      {
        state.submittedQrCodeText
          ? <img src={QRCodeApi.create(state.submittedQrCodeText)} />
          : null
      }
      <input
        type="text"
        placeholder="input your qrcode text"
        onChange={evt => dispatch(SetQRCodeText(evt.currentTarget.value))}
      />
      <button onClick={_ => dispatch(CreateQRCode)}>Generate</button>
      <Camera.View dispatch={dispatch} />
    </div>

  )
}

export const View = Dashboard

