
export const open = (url: string) =>
  window.open(url)

export const accessCamera = (video: HTMLVideoElement) =>
  navigator.mediaDevices.getUserMedia({video: true})
    .then((stream) => {
      video.src = URL.createObjectURL(stream)
      video.play()
    })
