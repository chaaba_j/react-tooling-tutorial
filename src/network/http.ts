type QueryParams = { [key: string]: string }

type Parser = (json: any) => any

// Need to extend from Error to have a stacktrace
export class HttpError extends Error {
  public status: number

  constructor(status: number) {
    super()
    this.name = "HttpError"
    this.status = status
  }
}

export interface Query {
  uri: string
  params: QueryParams,
  parser?: Parser
}

export const query = (uri: string, params: QueryParams = {}, parser?: Parser): Query => {
  return {uri, params, parser}
}

const headers = () => new Headers({
  "Content-Type": "application/json"
})

export const get = (query: Query): Promise<any> =>
  fetch(query.uri, {
    headers: headers(),
    method: "GET"
  }).then((response) => {
    if (response.ok)
      return response.json()
    throw new HttpError(response.status)
  })

export const post = (query: Query, payload: any): Promise<any> =>
  fetch(query.uri, {
    method: "POST",
    headers: headers(),
    body: JSON.stringify(payload)
  }).then((response) => {
    if (response.ok)
      return response.json()
    throw new HttpError(response.status)
  })
