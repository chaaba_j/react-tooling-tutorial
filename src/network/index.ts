import {AnyAction} from "react-tooling"
import * as Http from "./http"
import {Dispatch} from "react-tooling"
import {Query} from "./http"

enum ActionType {
  Get = "Get",
  Post = "Post"
}

export interface Get {
  type: ActionType.Get,
  query: Http.Query,
  payload?: any,
  dispatch: Dispatch,
  onSuccess(data: any): AnyAction,
  onError: AnyAction,
  noReplay: true
}
export const Get = (query: Http.Query,
                    dispatch: Dispatch,
                    onSuccess: (data: any) => AnyAction,
                    onError: AnyAction): Get => {
  return {
    type: ActionType.Get,
    query,
    dispatch,
    onSuccess,
    onError,
    noReplay: true
  }
}

export interface Post {
  type: ActionType.Post,
  query: Http.Query,
  data: Maybe<JSON>,
  dispatch: Dispatch,
  onSuccess(data: Maybe<any>): AnyAction,
  onError: AnyAction,
  noReplay: true
}
export const Post =  (query: Http.Query,
                      data: Maybe<any>,
                      dispatch: Dispatch,
                      onSuccess: (data: any) => AnyAction,
                      onError: AnyAction) => {
  return {
    type: ActionType.Post,
    query,
    data,
    dispatch,
    onSuccess,
    onError,
    noReplay: true
  }
}

export type Action = Get | Post

export const reactsTo = (action: AnyAction): action is Action => {
  switch (action.type) {
    case ActionType.Get:
    case ActionType.Post:
      return true
    default:
      return false
  }
}

const parseData = (query: Query, json: any): any =>
  query.parser ? query.parser(json) : json

const handleRequest = (promise: Promise<any>, action: Action): any =>
  promise.then((json) => {
    action.dispatch(
      action.onSuccess(
        parseData(action.query, json)
      )
    )
  }).catch((_: any) => action.dispatch(action.onError))

export const update = (action: Action) => {
  switch (action.type) {
    case ActionType.Get: {
      handleRequest(Http.get(action.query), action)
      break
    }
    case ActionType.Post: {
      handleRequest(Http.post(action.query, action.data), action)
      break
    }
  }
}

