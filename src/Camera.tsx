import {Component, React, Dispatcher} from "react-tooling"
import * as Browser from "./browser"

export enum ActionType {
  CaptureStarted = "CaptureStarted",
  CaptureFailed = "CaptureFailed"
}

export interface CaptureStarted {
  type: ActionType.CaptureStarted
}
export const CaptureStarted: CaptureStarted = {type: ActionType.CaptureStarted}

export interface CaptureFailed {
  type: ActionType.CaptureFailed
}
export const CaptureFailed: CaptureFailed = {type: ActionType.CaptureFailed}

export type Action = CaptureStarted

export type CameraProps = Dispatcher

/*
  When we need to access to the dom element we cannot use
  stateless component. We have to use stateful component instead
 */
class Camera extends Component<CameraProps> {
  ref: React.RefObject<HTMLVideoElement>

  constructor(props: CameraProps) {
    super(props)
    this.ref = React.createRef()
  }

  componentDidMount() {
    if (this.ref.current) {
      Browser.accessCamera(this.ref.current)
        .then(() => this.props.dispatch(CaptureStarted))
        .catch(() => this.props.dispatch(CaptureFailed))
    }
  }

  render() {
    return (
      <video id="video" width="640" height="480" ref={this.ref}>
      </video>
    )
  }
}

export const View = Camera
