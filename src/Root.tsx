/*
  This is a component file
  A component files consist of
  actions, reactsTo, update, view

  actions is a list of action that the component care about
  reactsTo is a function to filter only the action that the component care about
  update as his name means update the state.

  cycle of execution

  dispatch(action) -> update state -> render
  action -> update -> render
 */
import {Route, dashboard, RouteType} from "routes"
import { React, Dispatcher, Dispatch, AnyAction,
  Goto, GotoType, InitType, Init } from "react-tooling"
import {Lang} from "lang"
import * as Dashboard from "./Dashboard"
import * as Network from "./network"

export interface State {
  route: Route,
  dashboard: Dashboard.State
  lang: Lang
}

export const initState: State = {
  route: dashboard,
  dashboard: Dashboard.initState,
  lang: Lang.en
}

export enum ActionType {
  Loaded = "Loaded"
}

export interface Loaded {
  type: ActionType.Loaded
}
export const Loaded: Loaded = {type: ActionType.Loaded}

export type Action =
  Init |
  Goto<Route> |
  Loaded |
  Network.Action |
  Dashboard.Action

export const reactsTo = (action: AnyAction): action is Action => {
  switch (action.type) {
    case InitType:
    case GotoType:
    case ActionType.Loaded:
      return true
    default:
      return Dashboard.reactsTo(action) ||
        Network.reactsTo(action)
  }
}

export const update =
  (state: State, action: Action, dispatch: Dispatch) => {
    switch (action.type) {
      case InitType:
        dispatch(Loaded)
        return state
      case ActionType.Loaded:
        return state
      case GotoType:
        return state
      default: {
        if (Dashboard.reactsTo(action)) {
          return {
            ...state,
            dashboard: Dashboard.update(state.dashboard, action)
          }
        } else if (Network.reactsTo(action)) {
          // execute network request
          // network never update the state
          Network.update(action)
          return state
        }
        return state
      }
    }
  }

type Root = (props: State & Dispatcher) => JSX.Element

const Root: Root = ({lang, dispatch, ...state}) =>
  <div className="root">
  {(() => {
    // Routing
    switch (state.route.type) {
      case RouteType.Dashboard:
        return <Dashboard.View
          dispatch={dispatch}
          lang={lang}
          {...state.dashboard} />
      case RouteType.NotFound:
        return <div className="not-found">Not found</div>
    }
  })()}
  </div>

export const view = Root
