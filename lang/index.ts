import en from "lang/en"
import ja from "lang/ja"

export enum Lang {
  en = "en",
  ja = "ja"
}

export type Localized = {lang: Lang}

export type Translation = typeof en

export const translation = (lang: Lang) => {
  switch (lang) {
    case Lang.en: return en
    case Lang.ja: return ja
  }
}
export const parseLanguage = (lang: string): Lang => {
  const japanese = ["ja", "JP", "ja", "ja", "日本語", "ISO-2022-JP"]
  const english = ["en", "US", "en", "en", "English", "UTF-8"]
  if (japanese.indexOf(lang) !== -1)
    return Lang.ja
  else if (english.indexOf(lang) !== -1)
    return Lang.en
  return Lang.ja
}
