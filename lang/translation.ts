import en from "./en"
import identity from "lodash-es/identity"

export const translation: (it: typeof en) => typeof en = identity
