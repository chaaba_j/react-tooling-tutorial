// This is a hack to get around the following error:

// RUNTIME EXCEPTION  Exception occurred while loading your tests
// Invariant Violation: Browser history needs a DOM

// Mocha USED to work without this. Jalal and Ben don't know why it suddenly
// broke. Perhaps we will investigate later but this is good enough for now.

require("jsdom-global")()
