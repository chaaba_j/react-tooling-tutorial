type Nothing = null | undefined | void
type Maybe<T> = T | Nothing
type List<T> = ReadonlyArray<T>
type Lazy<T> = (x: T) => T
